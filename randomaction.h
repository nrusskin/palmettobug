#ifndef __RANDOM_ACTION_H__
#define __RANDOM_ACTION_H__

#include "visitor.h"

/*! Random unit movement */
class RandomAction : public IVisitor
{
public:
	RandomAction();
	virtual ~RandomAction();
	virtual void operator() (Unit* unit);
private:
	short getRandomOffset() const;	/*!< \return {-1,0,1} */

};

#endif//__RANDOM_ACTION_H__
