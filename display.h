#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include "visitor.h"
#include "types.h"

#define DISPLAY_HEIGHT (25)
#define DISPLAY_WIDTH (79)


/*!
 * Show unit on the console display
 */
class Display : public IVisitor
{
public:
	Display();
	virtual ~Display();
	void operator() (Unit* unit);
private:
	void gotoxy(const Position& pos);		/*!< move caret */
	bool isVisible(const Position& pos);	/*!< unit is in display rect*/
	void draw(const Position& pos, icon i);	/*!< display unit*/
};

#endif//__DISPLAY_H__
