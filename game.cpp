#include "game.h"
#include "world.h"
#include "unit.h"
#include "unitfactory.h"
#include "display.h"
#include "keyboard.h"

#define ESCAPE (27)

void Game::display(World* world)
{
	world->for_each(Display());
}

void Game::move(World* world)
{
	world->for_each([](Unit* unit) {unit->move(); });
}

int Game::run()
{
	World world;
	UnitFactory factory;

	world.addUnit(factory.createHuman({ 15, 7 }));
	world.addUnit(factory.createBug({40, 10}));
	world.addUnit(factory.createBug({20, 7}));
	world.addUnit(factory.createBug({60, 15}));

	do
	{
		display(&world);
		move(&world);
	} while(Keyboard::get_last_char() != ESCAPE);

	return 0;
}
