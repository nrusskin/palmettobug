#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

/*! Keyboard driver */
class Keyboard
{
public:
	/*< get keyboard character */
	static int get_ch();
	static int get_last_char();
private:
	static int last_char;
};

#endif//__KEYBOARD_H__