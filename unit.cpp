#include "unit.h"
#include "visitor.h"

Unit::Unit(const Position& pos, icon icon, ActoinPtr action)
	: m_pos(pos)
	, m_icon(icon)
	, m_action(action)
{
}

Unit::~Unit()
{
}

const Position& Unit::getPosition() const
{
	return m_pos;
}

void Unit::setPosition(const Position& pos)
{
	m_pos = pos;
}

icon Unit::getIcon() const
{
	return m_icon;
}

void Unit::move()
{
	(*m_action)(this);
}
