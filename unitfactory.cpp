#include "unitfactory.h"
#include "unit.h"
#include "consoleaction.h"
#include "randomaction.h"

UnitPtr UnitFactory::createHuman(const Position& pos)
{
	return create(pos, HUMAN, std::make_shared<ConsoleAction>());
}

UnitPtr UnitFactory::createBug(const Position& pos)
{
	return create(pos, BUG, std::make_shared<RandomAction>());
}

UnitPtr UnitFactory::create(const Position& pos, icon i, ActoinPtr action)
{
	return UnitPtr(new Unit(pos, i, action));
}
