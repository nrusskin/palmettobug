#include <stdio.h>
#include "display.h"
#include "types.h"
#include "unit.h"

#ifdef LINUX
void Display::gotoxy(const Position& pos)
{
	printf("%c[%d;%df", 0x1B, pos.y, pos.x);
}
#else//LINUX
#include <windows.h>

void Display::gotoxy(const Position& pos)
{
	COORD coord;
	coord.X = pos.x;
	coord.Y = pos.y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}
#endif//LINUX

bool Display::isVisible(const Position& pos)
{
	return (0 <= pos.x && pos.x < DISPLAY_WIDTH &&
		0 <= pos.y && pos.y < DISPLAY_HEIGHT);
}

void Display::draw(const Position& pos, icon i)
{
	gotoxy(pos);
	printf("%c", i);
}

void Display::operator() (Unit* unit)
{
	Position pos = unit->getPosition();

	if (isVisible(pos))
	{
		draw(pos, unit->getIcon());
	}
}

Display::Display()
{
	gotoxy({ 0, 0 });
	// clear display
	for (int i = 0; i < DISPLAY_HEIGHT; ++i)
		printf("%*c\n", DISPLAY_WIDTH, ' ');
	// help
	printf("Move:WASD/QEZXC\tExit:ESC");
}

Display::~Display()
{
	gotoxy({ DISPLAY_WIDTH, DISPLAY_HEIGHT });
}
