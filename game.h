#ifndef __GAME_H__
#define __GAME_H__

class World;

/*!< game base class */
class Game
{
public:
	int run();					/*!< game loop */
private:
	void display(World* world);	/*!< display game units */
	void move(World* world);	/*!< process unit movement*/
};

#endif//__GAME_H__
