#include "keyboard.h"

#ifdef LINUX
#include <stdio.h>
#include <sys/ioctl.h> // For FIONREAD
#include <termios.h>
#include <stdbool.h>

int kb_hit(void) {
    static bool initflag = false;
    static const int STDIN = 0;

    if (!initflag) {
        // Use termios to turn off line buffering
        struct termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initflag = true;
    }

    int nbbytes;
    ioctl(STDIN, FIONREAD, &nbbytes);  // 0 is STDIN
    return nbbytes;
}

int Keyboard::get_ch()
{
	kb_hit();
	last_char = getchar();
	return last_char;
}

#else//LINUX

#include <conio.h>

int Keyboard::get_ch()
{
	while (!_kbhit());
	last_char = _getch();
	return last_char;
}

#endif//LINUX

int Keyboard::last_char = -1;

int Keyboard::get_last_char()
{
	return last_char;
}