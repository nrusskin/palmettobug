#ifndef __WORLD_H__
#define __WORLD_H__

#include <list>
#include <algorithm>	// std::for_each
#include "types.h"

/*! World instance */
class World
{
public:
	
	template <typename Function >	/*!< process each world unit */
	void for_each(Function fn)
	{
		for (auto& unit : m_units)
		{
			fn(unit.get());
		}
	}

	void addUnit(UnitPtr unit);		/*!< add unit to the world */

private:
	typedef std::list<UnitPtr> UnitList;

	UnitList m_units;				/*!< units storage */
};

#endif//__WORLD_H__
