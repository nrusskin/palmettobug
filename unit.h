#ifndef __UNIT_H__
#define __UNIT_H__

#include "types.h"


/*! \class Unit
 * Game unit
 */
class Unit
{
public:
	/*!
	 * @param[in] x position
	 * @param[in] y position
	 * @param[in] action movement policy
	 */
	Unit(const Position& pos, icon icon, ActoinPtr action);
	~Unit();

	const Position& getPosition() const;
	void setPosition(const Position& pos);
	
	icon getIcon() const;

	void move();			/*!< move unit */

private:
	Position	m_pos;		/*!< position */
	icon		m_icon;		/*!< dispaly symbol */
	ActoinPtr	m_action;	/*!< action policy */
};

#endif//__UNIT_H__
