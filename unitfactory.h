#ifndef __UNIT_FACTORY_H__
#define __UNIT_FACTORY_H__

#include "types.h"

/*!< Game unit factory */
class UnitFactory
{
public:
	UnitPtr createHuman(const Position& pos);
	UnitPtr createBug(const Position& pos);

private:
	/*!
	 * \param[in] x position
	 * \param[in] y position
	 * \param[in] icon unit display symbol
	 * \param[in] action movement strategy
	 * \return Unit shared_ptr
	 */
	UnitPtr create(const Position& pos, icon icon, ActoinPtr action);
};

#endif//__UNIT_FACTORY_H__