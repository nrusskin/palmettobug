#ifndef __VISITOR_H__
#define __VISITOR_H__

class Unit;

/*! Visitor design pattern interface */
class IVisitor
{
public:
	virtual ~IVisitor() {}
	virtual void operator() (Unit* unit) = 0;	/*!< process unit */
};

#endif//__VISITOR_H__
