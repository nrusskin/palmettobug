#ifndef __CONSOLE_ACTION_H__
#define __CONSOLE_ACTION_H__

#include "visitor.h"
#include "types.h"

/*!
 *	Movement based on the user input (console input)
 */
class ConsoleAction : public IVisitor
{
public:
	virtual ~ConsoleAction();
	virtual void operator()(Unit* unit);
private:
	/*! evaluate next position
	 * \param[in] pos current position (copy by value)
	 * \return new position
	 */
	Position getNextPosition(Position pos) const;
};

#endif//__CONSOLE_ACTION_H__
