#include "consoleaction.h"
#include "unit.h"
#include "keyboard.h"

ConsoleAction::~ConsoleAction()
{
}

void ConsoleAction::operator()(Unit* unit)
{
	Position pos = unit->getPosition();
	unit->setPosition(getNextPosition(pos));
}

Position ConsoleAction::getNextPosition(Position p) const
{
	int control = Keyboard::get_ch();
	switch (control)
	{
	case 'w':
		--p.y;
		break;
	case 's':
		++p.y;
		break;
	case 'a':
		--p.x;
		break;
	case 'd':
		++p.x;
		break;
	case 'q':
		--p.x;
		--p.y;
		break;
	case 'e':
		++p.x;
		--p.y;
		break;
	case 'c':
	case 'x':
		++p.x;
		++p.y;
		break;
	case 'z':
		--p.x;
		++p.y;
		break;
	default:
		break;
	}

	return p;
}
