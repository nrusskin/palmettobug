#include <time.h>
#include <stdlib.h>
#include "randomaction.h"
#include "unit.h"

RandomAction::RandomAction()
{
	srand((unsigned int)time(NULL));
}

RandomAction::~RandomAction()
{
}

short RandomAction::getRandomOffset() const
{
	return rand()%3 - 1;
}

void RandomAction::operator() (Unit* unit)
{
	Position pos = unit->getPosition();
	pos.x += static_cast<Position::x_pos>(getRandomOffset()); 
	pos.y += static_cast<Position::y_pos>(getRandomOffset());
	unit->setPosition(pos);
}
