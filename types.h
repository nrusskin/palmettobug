#ifndef __TYPES_H__
#define __TYPES_H__

#include <memory>	// std::shared_ptr

struct Position
{
	typedef short x_pos;
	typedef short y_pos;

	x_pos	x;
	y_pos	y;
};

typedef enum {
	BUG = 248,
	HUMAN = 219
} icon;

class Unit;
class IVisitor;

typedef std::shared_ptr<Unit> UnitPtr;
typedef std::shared_ptr<IVisitor> ActoinPtr;


#endif// __TYPES_H__