CC=clang++
CFLAGS=-std=c++11 -Wall -g -D LINUX
SOURCES=*.cpp
EXECUTABLE=palmettobug

.PHONY: all clean

all: $(EXECUTABLE)
	
$(EXECUTABLE): $(SOURCES) 
	$(CC) $(CFLAGS) $(SOURCES) -o $@

clean:
	rm $(EXECUTABLE)
